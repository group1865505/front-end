import React, { useEffect, useState } from 'react';


import axios from 'axios';
import ProductCard from './ProductCard';

const CourseApp = () => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetchProducts();
  }, []);

  const fetchProducts = async () => {
    try {
      const response = await axios.get('http://localhost:5002/api/produits');
    
      setProducts(response.data);
    } catch (error) {
      console.error('Error fetching products:', error);
    }
  };

  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
        <div className="container">
          <div className="navbar-brand d-flex align-items-center">
            <img src="shopping-cart.jpg" alt="Logo" className="navbar-logo mr-2" style={{ width: '40px', height: '40px' }}/>
            <span className="navbar-title" style={{ fontSize: '35px', marginLeft: '50px', fontFamily: ' Oswald'}}>Mcommerce</span>
          </div>
        </div>
      </nav>
      <div className="container mt-4 mb-4">
      <div className="row">
        {products.map((product) => (
          <div key={product._id} className="col-md-4 mb-4">
            <ProductCard product={product} />
          </div>
        ))}
      </div>
    </div>
    </div>
    
  );
};
export default CourseApp;


