
import React, { useState } from 'react';
import axios from 'axios';

const ProductCard = ({ product }) => {
  const { _id, titre, image, description } = product;
  const [showDetails, setShowDetails] = useState(false);
  const [commandeSuccess, setCommandeSuccess] = useState(false);
  const [paiementSuccess, setPaiementSuccess] = useState(false);
  const [commandeError, setCommandeError] = useState(false);
  const [paiementError, setPaiementError] = useState(false);
  const [commandeId, setCommandeId] = useState("");

  const handleDetailsClick = () => {
    setShowDetails(true);
  };

  const handleCommanderClick = async () => {
    try {
      const response = await axios.post('http://localhost:5000/api/commandes', {
        productId: _id,
      });

      if (response.status === 201) {
        setCommandeId(response.data._id)
        setCommandeSuccess(true);
        setCommandeError(false);
      } else {
        setCommandeSuccess(false);
        setCommandeError(true);
      }
    } catch (error) {
      console.error('Error making "Commander" API call:', error);
      setCommandeSuccess(false);
      setCommandeError(true);
    }
  };

  const handlePayerClick = async () => {
    try {
      console.log(_id)
      const response = await axios.post('http://localhost:5001/api/paiement', {
        commandeId: commandeId,
      });

      if (response.status === 201) {
        setPaiementSuccess(true);
        setPaiementError(false);
      } else {
        setPaiementSuccess(false);
        setPaiementError(true);
      }
    } catch (error) {
      console.error('Error making "Payer" API call:', error);
      setPaiementSuccess(false);
      setPaiementError(true);
    }
  };

  const handleClose = () => {
    setShowDetails(false);
    setCommandeSuccess(false);
    setCommandeError(false);
    setPaiementSuccess(false);
    setPaiementError(false);
  };

  return (
    <div className="col-lg-3 col-md-4 col-sm-6">
      <div className="card h-100 shadow-sm rounded" style={{ width: '300px', height: '300px' }}>
        <div className="card shadow-sm rounded">
          <img src={image} alt={titre} className="card-img-top product-card-image" />
          <div className="card-body">
            <h5 className="card-title">{titre}</h5>
            <button className="btn btn-primary" onClick={handleDetailsClick}>
              Details
            </button>
          </div>
        </div>
      </div>

      {showDetails && (
        <div className="modal" tabIndex="-1" role="dialog" style={{ display: 'block' }}>
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">{titre}</h5>
              </div>
              <div className="modal-body">
                <img src={image} alt={titre} className="product-details-image" />
                <p>{description}</p>
                {commandeSuccess ? (
                  <div className="alert alert-success">Commande ajoutée avec succès</div>
                ) : (
                  <>
                    <button className="btn btn-primary" onClick={handleCommanderClick}>
                      Commander
                    </button>
                    {commandeError && (
                      <div className="alert alert-danger">Échec de l'ajout de la commande</div>
                    )}
                  </>
                )}
              </div>
              <div className="modal-footer">
                {commandeSuccess && !paiementSuccess && !paiementError && (
                  <>
                    <p className="text-primary fs-5">Quantité: 1</p>
                    <button className="btn btn-primary" onClick={handlePayerClick}>
                      Payer
                    </button>
                  </>
                )}
                {paiementError && (
                  <div className="alert alert-danger w-100">Échec de paiement</div>
                )}
                <button type="button" className="btn btn-secondary" onClick={handleClose}>
                  Close
                </button>
              </div>
              {paiementSuccess && (
                <div className="alert alert-success" style={{ marginBottom: 0 }}>
                  Paiement accepté
                </div>
              )}
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default ProductCard;

