import React from 'react';
import ProductList from './components/ProductList';
import 'bootstrap/dist/css/bootstrap.min.css';


const App = () => {
  return (
    <div>
      
      <ProductList />

    </div>
  );
};

export default App;

